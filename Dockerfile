FROM debian:stable

ARG UNAME=hacker
ARG UID=1000
ARG GID=1000

RUN export DEBIAN_FRONTEND="noninteractive" && \
    apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y dist-upgrade && \
    apt-get -y autoremove && \
    apt-get -y install \
        python3-pip

RUN groupadd -g $GID -o $UNAME
RUN adduser \
    --gecos '' \
    --disabled-password \
    --uid $UID \
    --gid $GID \
    $UNAME

USER $UNAME
WORKDIR /home/$UNAME
ENV PATH /home/$UNAME/.local/bin:/usr/bin:/bin

RUN pip3 install \
    sphinx_rtd_theme \
    sphinx \
    sphinx-autobuild \
    sphinx_fontawesome

RUN mkdir /home/$UNAME/zcash-docs
WORKDIR /home/$UNAME/zcash-docs
VOLUME /home/$UNAME/zcash-docs
ENTRYPOINT ["make"]
