#!/bin/bash
#
# Assuming docker is installed on a gnu/linux system, this will render the documentation into ./build.

set -efuxo pipefail
declare -a ARGS

ARGS=('html')
if [ $# -gt 0 ]
then
    ARGS="$@"
fi

# Change directory to the source repo root to ensure executing this script from any path will work:
cd "$(dirname "$(readlink -f "$0")")"

docker build \
       -t zcash-docs \
       --build-arg UID="$(id -u)" \
       --build-arg GID="$(id -g)" \
       .

docker run \
       --mount "type=bind,source=$(pwd),target=/home/hacker/zcash-docs/" \
       zcash-docs \
       "${ARGS[@]}"
